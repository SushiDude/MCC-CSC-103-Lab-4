/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-11-18 */
package edu.monroecc.student.jruiz13ANDwbrown49.lab4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/** The class {@code Lab4} reads input data from a file in order to run a
 * {@code RunwaySimulation}.
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @see RunwaySimulation */
public class Lab4
{
	/** Reads data from the input file and passes it to a RunwaySimulation.
	 *
	 * @param args an array of Strings representing the command line arguments.
	 * @throws IOException if the input file can not be read.
	 * @see RunwaySimulation#runwaySimulate(int, int, int, int, int, int) */
	public static void main(final String[] args) throws IOException
	{
		// Open the file and begin processing as input.
		final BufferedReader bufferedReader =
				new BufferedReader(new FileReader("input.txt"));
		for (String line = bufferedReader.readLine(); line != null; line =
				bufferedReader.readLine())// Read file line by line.
		{
			final String arguments[] = line.split("\\s+|,");
			RunwaySimulation.runwaySimulate(Integer.parseInt(arguments[0]),
					Integer.parseInt(arguments[1]),
					Integer.parseInt(arguments[2]),
					Integer.parseInt(arguments[3]),
					Integer.parseInt(arguments[4]),
					Integer.parseInt(arguments[5]));
		}
		bufferedReader.close();// Close the file.
		System.exit(0);// Exit cleanly.
	}
}
