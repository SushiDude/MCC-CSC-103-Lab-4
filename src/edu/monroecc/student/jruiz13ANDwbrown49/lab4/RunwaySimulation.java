/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-11-18 */
package edu.monroecc.student.jruiz13ANDwbrown49.lab4;

import edu.colorado.collections.LinkedQueue;
import edu.colorado.collections.LinkedStack;
import edu.colorado.simulations.Averager;
import edu.colorado.simulations.BooleanSource;

/** The class {@code RunwaySimulation} implements methods for simulating a plane
 * runway.
 *
 * @author Fig
 * @author Wyatt J. Brown
 * @see Plane
 * @see Runway */
public class RunwaySimulation
{
	/** Simulates a plane runway and prints results to the standard output
	 * stream.
	 *
	 * @param landingTime the amount of time in minutes needed for one plane to
	 *        land.
	 * @param takeoffTime the amount of time in minutes needed for one plane to
	 *        take off.
	 * @param averageLandingArrivalTime the average amount of time in minutes
	 *        between arrival of planes to the landing queue.
	 * @param averageTakeoffArrivalTime the average amount of time in minutes
	 *        between arrival of planes to the takeoff queue.
	 * @param crashTime the maximum amount of time in minutes that a plane can
	 *        stay in the landing queue without running out of fuel and
	 *        crashing.
	 * @param totalTime the total length of time in minutes to be simulated. */
	public static void runwaySimulate(final int landingTime,
			final int takeoffTime, final int averageLandingArrivalTime,
			final int averageTakeoffArrivalTime, final int crashTime,
			final int totalTime)
	{
		System.out.println(
				"The time of simulation is:\t" + totalTime + " minutes");
		System.out.println(
				"The amount of time that is needed for one plane to take off is:\t"
						+ takeoffTime + " minutes");
		System.out.println(
				"The amount of time that is needed for one plane to land is:\t"
						+ landingTime + " minutes");
		System.out.println(
				"The average amount of time between arrival of planes to the takeoff queue is\t"
						+ averageTakeoffArrivalTime + " minutes");
		System.out.println(
				"The average amount of time between arrival of planes to the landing queue is\t"
						+ averageLandingArrivalTime + " minutes");
		System.out.println(
				"The maximum time a plane can stay in the landing queue before crashing is\t"
						+ crashTime + " minutes");

		final Runway runway = new Runway(takeoffTime, landingTime);

		final Averager landingAverager = new Averager();
		final Averager takeoffAverager = new Averager();

		final BooleanSource landingBooleanSource = new BooleanSource(
				(double) averageLandingArrivalTime / totalTime);
		final BooleanSource takeoffBooleanSource = new BooleanSource(
				(double) averageTakeoffArrivalTime / totalTime);

		final LinkedQueue<Plane> landingLinkedQueue = new LinkedQueue<Plane>();
		final LinkedQueue<Plane> takeoffLinkedQueue = new LinkedQueue<Plane>();

		final LinkedStack<Plane> crashLinkedStack = new LinkedStack<Plane>();
		final LinkedStack<Integer> crashTimeLinkedStack =
				new LinkedStack<Integer>();

		int landingArrivalNumber = 0;
		int takeoffArrivalNumber = 0;

		Plane currentPlane = null;

		int remainingTime = 0;

		for (int currentMinute = 1; currentMinute <= totalTime; ++currentMinute)
		{
			System.out.println("min " + currentMinute + " :");

			// Make planes arrive for takeoff.
			System.out.print("\tArrived for Takeoff : ");
			if (takeoffBooleanSource.query())
			{
				final Plane plane = new Plane(currentMinute, 'T');
				takeoffLinkedQueue.add(plane);
				++takeoffArrivalNumber;
				System.out.println("Plane #" + plane.getPlaneNo());
			} else
				System.out.println();

			// Make planes arrive for landing.
			System.out.print("\tArrived for Landing : ");
			if (landingBooleanSource.query())
			{
				final Plane plane = new Plane(currentMinute, 'L');
				landingLinkedQueue.add(plane);
				++landingArrivalNumber;
				System.out.println("Plane #" + plane.getPlaneNo());
			} else
				System.out.println();

			// Handle which plane gets to use the runway.
			// Landing planes get priority.
			while (!runway.isBusy() && !landingLinkedQueue.isEmpty())
			{
				currentPlane = landingLinkedQueue.remove();
				// If the plane can not land in time, make it crash.
				if (currentMinute - currentPlane.getTime() > crashTime)
				{
					crashLinkedStack.push(currentPlane);
					crashTimeLinkedStack.push(currentMinute);
				} else
				{
					runway.startUsingRunway(currentPlane.getOperation());
					landingAverager
							.addNumber(currentMinute - currentPlane.getTime());
					remainingTime = landingTime;
				}
			}

			// If there are no planes to land, make planes take off.
			if (!runway.isBusy() && !takeoffLinkedQueue.isEmpty())
			{
				currentPlane = takeoffLinkedQueue.remove();
				runway.startUsingRunway(currentPlane.getOperation());
				takeoffAverager
						.addNumber(currentMinute - currentPlane.getTime());
				remainingTime = takeoffTime;
			}

			// Print the current state of the runway.
			System.out.print("\tRunway : ");
			if (runway.isBusy())
			{
				System.out
						.print("Plane #" + currentPlane.getPlaneNo() + " is ");
				switch (currentPlane.getOperation())
				{
				case 'L':
					System.out.print("landing");
					if (remainingTime == 1)
						System.out.print(" (landing finished)");
					break;
				case 'T':
					System.out.print("taking off");
					if (remainingTime == 1)
						System.out.println(" (takeoff finished)");
					break;
				}
				System.out.println();
			} else
				runway.startUsingRunway('I');
			if (runway.typeOfOperation() == 'I')
				System.out.println("Idle");
			System.out.println();

			--remainingTime;
			runway.reduceRemainingTime();
		}

		System.out
				.println("No of planes that came to the runway for takeoff\t:\t"
						+ takeoffArrivalNumber);
		System.out
				.println("No of planes that came to the runway for landing\t:\t"
						+ landingArrivalNumber);
		System.out.println("No of planes that crashed\t\t\t\t:\t"
				+ crashLinkedStack.size());
		System.out.println(
				"The average time a plane spent on the takeoff queue is\t:\t"
						+ takeoffAverager.average() + " min");
		System.out.println(
				"The average time a plane spent on the landing queue is\t:\t"
						+ landingAverager.average() + " min");
		while (!crashLinkedStack.isEmpty())
			System.out.println("plane " + crashLinkedStack.pop().getPlaneNo()
					+ " crashed at minute " + crashTimeLinkedStack.pop());
	}
}
