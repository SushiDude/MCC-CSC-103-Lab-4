/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-11-18 */
package edu.monroecc.student.jruiz13ANDwbrown49.lab4;

public class Plane
{
	static private int planeCount = 0;

	/** @return an int representing the number of Planes created. */
	public static int getPlaneCount()
	{
		return planeCount;
	}

	private final char operation;
	private final int planeNo;
	private final int time;

	/** Constructs a plane with a given time of arrival to land or take off.
	 *
	 * @param time an int representing the time of arrival.
	 * @param landingOrTakeOff a char representing if the plane is [L]anding or
	 *        [T]aking off. */
	public Plane(final int time, final char landingOrTakeOff)
	{
		this.time = time;
		operation = landingOrTakeOff;
		planeNo = ++planeCount;
	}

	/** @return a char representing if the plane is [L]anding or [T]aking
	 *         off. */
	public char getOperation()
	{
		return operation;
	}

	/** @return an int representing a unique identifier for the given plane. */
	public int getPlaneNo()
	{
		return planeNo;
	}

	/** @return an int representing the time of arrival. */
	public int getTime()
	{
		return time;
	}
}
