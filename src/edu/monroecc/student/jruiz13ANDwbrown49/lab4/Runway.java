/* Written by Fig and Wyatt J. Brown.
 * Due date: 2015-11-18 */
package edu.monroecc.student.jruiz13ANDwbrown49.lab4;

public class Runway
{
	private char operation;// can be: I - Idle, L - Landing, T - Takeoff.
	private int runwayTimeLeft;
	private final int timeForLanding;
	private final int timeForTakeoff;

	/** Constructs a runway that has a given amount of time to take off and
	 * land.
	 *
	 * @param timeForTakeoff an int representing the time it takes to take off.
	 * @param timeForLanding an int representing the time it takes to land. */
	public Runway(final int timeForTakeoff, final int timeForLanding)
	{
		runwayTimeLeft = 0;
		this.timeForTakeoff = timeForTakeoff;
		this.timeForLanding = timeForLanding;
		operation = 'I';
	}

	/** @return a boolean representing whether the runway is busy or not. */
	public boolean isBusy()
	{
		return runwayTimeLeft > 0;
	}

	/** Reduces the time remaining for the current operation by one. */
	public void reduceRemainingTime()
	{
		--runwayTimeLeft;
	}

	/** Reconfigures the runway for a different type of use.
	 *
	 * @param a char representing if the runway should be [I]dle, used for
	 *        [L]anding, or for [T]akeoff. */
	public void startUsingRunway(final char typeOfUse)
	{
		operation = typeOfUse;
		switch (typeOfUse)
		{
		case 'I':
			runwayTimeLeft = 0;
			break;
		case 'L':
			runwayTimeLeft = timeForLanding;
			break;
		case 'T':
			runwayTimeLeft = timeForTakeoff;
			break;
		}
	}

	/** @return a char representing if the runway is [I]dle, being used for
	 *         [L]anding, or for [T]akeoff. */
	public char typeOfOperation()
	{
		return operation;
	}
}
